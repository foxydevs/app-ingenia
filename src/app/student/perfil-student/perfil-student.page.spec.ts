import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerfilStudentPage } from './perfil-student.page';

describe('PerfilStudentPage', () => {
  let component: PerfilStudentPage;
  let fixture: ComponentFixture<PerfilStudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilStudentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerfilStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
