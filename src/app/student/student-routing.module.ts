import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentPage } from './student.page';

const routes: Routes = [
  {
    path: '',
    component: StudentPage,
    children: [
      {
        path: 'dashboard-student',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./dashboard-student/dashboard-student.module').then( m => m.DashboardStudentPageModule)
          }
        ]
      },
      {
        path: 'cobros-student',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./cobros-student/cobros-student.module').then( m => m.CobrosStudentPageModule)
          }
        ]
      },
      {
        path: 'perfil-student',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./perfil-student/perfil-student.module').then( m => m.PerfilStudentPageModule)
          }
        ]
      },
      /*{
        path: 'sucursal-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./sucursal-admin/sucursal-admin.module').then( m => m.SucursalAdminPageModule)
          }
        ]
      },
      {
        path: 'estudiante-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./estudiante-admin/estudiante-admin.module').then( m => m.EstudianteAdminPageModule)
          }
        ]
      },*/
      {
        path: '',
        redirectTo: '/student/dashboard-student',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/student/dashboard-student',
    pathMatch: 'full'
  },
  {
    path: 'cobro-student/:id',
    loadChildren: () => import('./cobro-student/cobro-student.module').then( m => m.CobroStudentPageModule)
  },
  {
    path: 'perfil-student',
    loadChildren: () => import('./perfil-student/perfil-student.module').then( m => m.PerfilStudentPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentPageRoutingModule {}
