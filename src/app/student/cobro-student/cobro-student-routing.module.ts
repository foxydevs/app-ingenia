import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CobroStudentPage } from './cobro-student.page';

const routes: Routes = [
  {
    path: '',
    component: CobroStudentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CobroStudentPageRoutingModule {}
