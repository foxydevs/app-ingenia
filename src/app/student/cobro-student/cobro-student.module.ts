import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CobroStudentPageRoutingModule } from './cobro-student-routing.module';

import { CobroStudentPage } from './cobro-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CobroStudentPageRoutingModule
  ],
  declarations: [CobroStudentPage]
})
export class CobroStudentPageModule {}
