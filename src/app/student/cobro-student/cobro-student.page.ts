import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CursoService } from 'src/app/_service/curso.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { CobroService } from 'src/app/_service/cobro.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cobro-student',
  templateUrl: './cobro-student.page.html',
  styleUrls: ['./cobro-student.page.scss'],
})
export class CobroStudentPage implements OnInit {
  parameter:any;
  btnDisabled:boolean;
  myStyles = {
    "background-image": "url('https://bpresentacion.s3-us-west-2.amazonaws.com/Desarrollo/Ingenia.png')"
  }
  data:any = {
    cobros_cantidad: 1,
    cobros_cursos_id: 0,
    cobros_usu_id: +localStorage.getItem('currentId'),
    cobros_fecha: '',
    cobros_estado: 1
  }
  dataCurso:any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private mainService: CursoService,
    private secondService: CobroService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    this.data.cobros_cursos_id = this.parameter;
    this.getSingle(this.parameter)
  }

  public saveChanges() {
    this.data.cobros_fecha = new Date().toISOString().split("T")[0];
    this.create(this.data)
  }

  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      this.dataCurso = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public create(data:any) {
    this.secondService.create(data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Cobro Agregado', 'El cobro ha sido agregado exitosamente.');
      this.goToRoute('student/cobros-student')
      this.btnDisabled = false;
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

}
