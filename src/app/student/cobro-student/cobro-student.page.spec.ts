import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CobroStudentPage } from './cobro-student.page';

describe('CobroStudentPage', () => {
  let component: CobroStudentPage;
  let fixture: ComponentFixture<CobroStudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobroStudentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CobroStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
