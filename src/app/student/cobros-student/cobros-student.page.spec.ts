import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CobrosStudentPage } from './cobros-student.page';

describe('CobrosStudentPage', () => {
  let component: CobrosStudentPage;
  let fixture: ComponentFixture<CobrosStudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobrosStudentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CobrosStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
