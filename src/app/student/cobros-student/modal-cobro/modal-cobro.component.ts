import { Component, OnInit } from '@angular/core';
import { CobroService } from 'src/app/_service/cobro.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';
import { CursoService } from 'src/app/_service/curso.service';

@Component({
  selector: 'app-modal-cobro',
  templateUrl: './modal-cobro.component.html',
  styleUrls: ['./modal-cobro.component.scss'],
})
export class ModalCobroComponent implements OnInit {
  data:any;
  curso:any;
  administrador:any;
  parameter:any;

  constructor(
    private mainService: CobroService,
    private secondService: CursoService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.administrador = this.navParams.get('admin');
    this.parameter = this.navParams.get('value');
    console.log(this.parameter)
    this.getSingle(this.parameter);
  }


  
  closeModal() {
    this.modalController.dismiss();
  }


  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      console.log(res)
      this.data = res;
      if(res.cobros_cursos_id) {
        this.getSingleCurso(res.cobros_cursos_id)
      }
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public getSingleCurso(id:any) {
    this.secondService.getSingle(id)
    .subscribe((res)=> {
      console.log(res)
      this.curso = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public update(data:any, estado:any) {

    let datos = {
      cobros_id: data.cobros_id,
      cobros_cantidad: data.cobros_cantidad,
      cobros_cursos_id: data.cobros_cursos_id,
      cobros_usu_id: data.cobros_usu_id,
      cobros_fecha: data.cobros_fecha,
      cobros_estado: estado
    }

    console.log(datos)

    this.mainService.update(datos)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Cobro Actualizado', 'El cobro ha sido actualizado exitosamente.');
      this.getSingle(this.parameter)
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);      
    });
  }
}
