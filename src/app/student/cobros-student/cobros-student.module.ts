import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CobrosStudentPageRoutingModule } from './cobros-student-routing.module';

import { CobrosStudentPage } from './cobros-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CobrosStudentPageRoutingModule
  ],
  declarations: [CobrosStudentPage]
})
export class CobrosStudentPageModule {}
