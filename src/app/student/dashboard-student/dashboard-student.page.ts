import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CursoService } from 'src/app/_service/curso.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-student',
  templateUrl: './dashboard-student.page.html',
  styleUrls: ['./dashboard-student.page.scss'],
})
export class DashboardStudentPage implements OnInit {
  table:any[];

  constructor(
    private modalController: ModalController,
    private router: Router,
    private mainService: CursoService,
    private notificationService: NotificacionService,
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  public getAll() {
    //this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      this.table = res;
      console.log(res)
      //this.notificationService.dismiss();
    }, (err) => {
      //this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }


}
