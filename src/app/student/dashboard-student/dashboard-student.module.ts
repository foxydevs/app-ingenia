import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardStudentPageRoutingModule } from './dashboard-student-routing.module';

import { DashboardStudentPage } from './dashboard-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardStudentPageRoutingModule
  ],
  declarations: [DashboardStudentPage]
})
export class DashboardStudentPageModule {}
