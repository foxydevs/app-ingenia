import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MetroTutorPage } from './metro-tutor.page';

describe('MetroTutorPage', () => {
  let component: MetroTutorPage;
  let fixture: ComponentFixture<MetroTutorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetroTutorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MetroTutorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
