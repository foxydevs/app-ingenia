import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MetroTutorPage } from './metro-tutor.page';

const routes: Routes = [
  {
    path: '',
    component: MetroTutorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MetroTutorPageRoutingModule {}
