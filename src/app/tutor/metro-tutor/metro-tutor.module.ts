import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MetroTutorPageRoutingModule } from './metro-tutor-routing.module';

import { MetroTutorPage } from './metro-tutor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MetroTutorPageRoutingModule
  ],
  declarations: [MetroTutorPage]
})
export class MetroTutorPageModule {}
