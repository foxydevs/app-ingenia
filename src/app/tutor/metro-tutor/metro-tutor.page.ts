import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

declare var $:any; //JQUERY

@Component({
  selector: 'app-metro-tutor',
  templateUrl: './metro-tutor.page.html',
  styleUrls: ['./metro-tutor.page.scss'],
})
export class MetroTutorPage implements OnInit {
  myStyles = {
    "background-image": "url('https://png.pngtree.com/png-vector/20191107/ourmid/pngtree-metronome-icon-flat-style-png-image_1952014.jpg')"
  }
  myStyles2 = {
    "background-image": "url('https://images.clipartlogo.com/files/istock/previews/1065/106549527-stopwatch-vector-icon.jpg')"
  }
  //PROPIEDADES
  selectItem:string = 'metronomo';

  //METRONOMO
  @ViewChild('audio', {static: false}) audio;
  @ViewChild('audio2', {static: false}) audio2;
  isPlaying:boolean = false;
  bpm:any = 100;
  tiempoReproduccion:any = 100;
  timer:any = null;

  //CRONOMETRO
  centesimas: number = 0;
  minutos: number = 59;
  segundos: number = 0;
  contador: any;
  _centesimas: string = '00';
  _minutos: string = '00';
  _segundos: string = '00';
  isRun = false;
  estado: string = 'play';
  refreshColor = 'light';

  //TEMPORIZADOR
  timeTemporizer:any = "00:00:00";
  isCount = false;
  _segundosT: string = '00';;
  _minutosT: string = '00';;
  _horasT: string = '00';;
  contadorT: any;
  timer2:any = null;

  constructor() { }

  ngOnInit() {
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  reproducir() {
    console.log(this.bpm)
    //const currentBpm = this.bpm;
    //console.log("BOOLENA", this.isPlaying)
    if(this.isPlaying) {
      //console.log("PAUSO", this.isPlaying)
      window.clearInterval(this.timer);

    } else {
      //console.log("SIGO", this.isPlaying)
      this.timer = window.setInterval(() => {
        this.audio.nativeElement.currentTime = 0;
        this.audio.nativeElement.play();
        //console.log("ENTRO A ", (60*1000)/this.bpm)
        //this.tiempoReproduccion = (60*1000)/this.bpm
      }, (60*1000)/this.bpm);
    }
    this.isPlaying = !this.isPlaying;
  }

  add() {
    if(this.bpm < 250) {
      this.bpm++;
    }
  }

  remove() {
    if(this.bpm > 40) {
      this.bpm--;
    }
  }

  
  estadoSwap() {
    this.isRun = !this.isRun;
    if (this.isRun) {
      this.refreshColor = 'gris';
      this.start();
    } else {
      this.refreshColor = 'light';
      this.pause();
    }
  }

  start() {
    this.contador = window.setInterval(() => {
      this.centesimas += 1;
      if (this.centesimas < 10) this._centesimas = '0' + this.centesimas;
      else this._centesimas = '' + this.centesimas;
      if (this.centesimas == 10) {
        this.centesimas = 0;
        this.segundos += 1;
        if (this.segundos < 10) this._segundos = '0' + this.segundos;
        else this._segundos = this.segundos + '';
        if (this.segundos == 60) {
          this.segundos = 0;
          this.minutos += 1;
          if (this.minutos < 10) this._minutos = '0' + this.minutos;
          else this._minutos = this.minutos + '';
          this._segundos = '00';
          if (this.minutos == 90) {
            this.pause();
          }
        }
      }
    }, 100)
  }

  pause() {
    window.clearInterval(this.contador);
  }

  stop() {
    if (!this.isRun) {
      window.clearInterval(this.contador);
      this.minutos = 0;
      this.segundos = 0;
      this.centesimas = 0;

      this._centesimas = '00';
      this._segundos = '00';
      this._minutos = '00';

      this.estado = 'play';
      this.isRun = false;
      this.contador = null;
    }
  }

  temporizerPlay() {
    this.isCount = !this.isCount;
    console.log(this.timeTemporizer.split(':'))

    this._horasT = this.timeTemporizer.split(':')[0];
    this._minutosT = this.timeTemporizer.split(':')[1];
    this._segundosT = this.timeTemporizer.split(':')[2];
    let audio = new Audio('../../../assets/audio/Claves.wav');

      //this.refreshColor = 'gris';
      //this.start();
      audio.pause();
      audio.currentTime = 0;

      console.log($("#Tiempo").val())
      $("#Tiempo").timer('remove');
      $("#Tiempo").timer({
        coutdown: true,
        duration: this._horasT+"h"+this._minutosT+"m"+this._segundosT+"s",
        callback: function() {
          audio.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
          }, false);
          audio.play();
          setTimeout(() => {
            audio.pause();
            $("#Tiempo").val('00:00:00');
          }, 3000);
        },
        format: '%H:%M:%S'
      });
  }

  startTemporizer() {
    ///this.contadorT = window.setInterval(() => {
      //this.audio.nativeElement.currentTime = 0;
      //this.audio.nativeElement.pause();
      /*console.log()
      console.log(this._horasT+"h"+this._minutosT+"m"+this._segundosT+"s");

      audio.pause();
      audio.currentTime = 0;

      console.log($("#Tiempo").val())
      $("#Tiempo").timer('remove');
      $("#Tiempo").timer({
        coutdown: true,
        duration: this._horasT+"h"+this._minutosT+"m"+this._segundosT+"s",
        callback: function() {
          audio.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
          }, false);
          audio.play();
          setTimeout(() => {
            audio.pause();
          }, 5000);
        },
        format: '%H:%M:%S'
      });
      

      //this._segundosT += 1;
      /*if (this.centesimas < 10) this._centesimas = '0' + this.centesimas;
      else this._centesimas = '' + this.centesimas;
      if (this.centesimas == 10) {
        this.centesimas = 0;
        this.segundos += 1;
        if (this.segundos < 10) this._segundos = '0' + this.segundos;
        else this._segundos = this.segundos + '';
        if (this.segundos == 60) {
          this.segundos = 0;
          this.minutos += 1;
          if (this.minutos < 10) this._minutos = '0' + this.minutos;
          else this._minutos = this.minutos + '';
          this._segundos = '00';
          if (this.minutos == 90) {
            this.pause();
          }
        }
      }*/
    //}, 1000)
  }

  finalizar() {
    $("#Tiempo").val('00:00:00')
  }


}
