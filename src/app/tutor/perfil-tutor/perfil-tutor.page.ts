import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil-tutor',
  templateUrl: './perfil-tutor.page.html',
  styleUrls: ['./perfil-tutor.page.scss'],
})
export class PerfilTutorPage implements OnInit {
  data = {
    picture: localStorage.getItem('currentPicture'),
    nombreCompleto: localStorage.getItem('currentFirstName') + " " + localStorage.getItem('currentLastName'),
    email: localStorage.getItem('currentEmail'),
  }
  myStyles = {
    "background-image": "url("+localStorage.getItem('currentPicture')+")"
  }
  constructor(
    private router:Router
    ) { }

  ngOnInit() {
  }

  logOut() {
    localStorage.clear();
    this.goToRoute('login')
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

}
