import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilTutorPageRoutingModule } from './perfil-tutor-routing.module';

import { PerfilTutorPage } from './perfil-tutor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilTutorPageRoutingModule
  ],
  declarations: [PerfilTutorPage]
})
export class PerfilTutorPageModule {}
