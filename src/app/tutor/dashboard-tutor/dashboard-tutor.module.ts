import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardTutorPageRoutingModule } from './dashboard-tutor-routing.module';

import { DashboardTutorPage } from './dashboard-tutor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardTutorPageRoutingModule
  ],
  declarations: [DashboardTutorPage]
})
export class DashboardTutorPageModule {}
