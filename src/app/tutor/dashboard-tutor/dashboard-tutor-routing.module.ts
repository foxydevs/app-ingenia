import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardTutorPage } from './dashboard-tutor.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardTutorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardTutorPageRoutingModule {}
