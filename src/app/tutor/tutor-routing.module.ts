import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TutorPage } from './tutor.page';

const routes: Routes = [
  {
    path: '',
    component: TutorPage,
    children: [
      {
        path: 'dashboard-tutor',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./dashboard-tutor/dashboard-tutor.module').then( m => m.DashboardTutorPageModule)
          }
        ]
      },
      {
        path: 'metro-tutor',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./metro-tutor/metro-tutor.module').then( m => m.MetroTutorPageModule)
          }
        ]
      },
      {
        path: 'perfil-tutor',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('./perfil-tutor/perfil-tutor.module').then( m => m.PerfilTutorPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'tutor/dashboard-tutor',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tutor/dashboard-tutor',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TutorPageRoutingModule {}
