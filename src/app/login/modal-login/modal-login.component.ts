import { Component, OnInit } from '@angular/core';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController } from '@ionic/angular';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { CursoService } from 'src/app/_service/curso.service';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss'],
})
export class ModalLoginComponent implements OnInit {
  //public applicationId:number = path.id;
  public title:string = 'Registro';
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private passwordType2:string = 'password';
  private passwordShow2:boolean = false;
  public btnDisabled = false;
  libros:any[];
  data:any = {
    usu_id: 0,
    usu_nombre: '',
    usu_apellido: '',
    usu_email: '',
    usu_matricula:'',
    usu_ocupacion_id: 0,
    usu_empresa:'',//
    usu_cargo:'',//
    usu_antiguedad:'',//
    usu_lugar_estudios:'',//
    usu_carrera:'',//
    usu_nivel_ano:'',//
    usu_direccion_casa: '',//
    usu_profesion: '',//
    usu_tel: '',//
    usu_cel: '',//
    usu_fech_nacimiento: '',//
    usu_inviertes_letras: '',//
    usu_dificultad_lectura: '',//
    usu_dificultad_atencion:'',//
    usu_problema_visual:'',//
    usu_problema_significado_oracion:'',//
    usu_usu_lees_por_que_id: 0,//
    usu_libros_anuales: '',//
    usu_lateralidad: '',//
    usu_capacidad_memoria:'',//
    usu_observaciones: '',
    usu_por_que:'',
    usu_para_que: '',
    usu_para_cuando:'',
    usu_fecha: '',
    usu_estado:1,
    usu_pass: '',
    usu_tipo: 3
  }
  dataCurso = {
    cursos_titulo:'Test 12:32',
    cursos_descripcion:'TeST 12:32',
    cursos_usu_id: 1,
    cursos_fecha: '2019-11-28',
    cursos_estado: 1
  }
  
  constructor(
    private usuarioService: UsuarioService,
    private mainService: CursoService,
    private notificationService: NotificacionService,
    private modalController: ModalController
    ) { }

  ngOnInit() {
    this.libros = [];
    for (let index = 0; index <= 100; index++) {
      this.libros.push(index);
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword2() {
    if(this.passwordShow2) {
      this.passwordShow2 = false;
      this.passwordType2 = 'password';
    } else {
      this.passwordShow2 = true;
      this.passwordType2 = 'text';
    }
  }

  saveChanges() {
    this.data.usu_fecha = new Date().toISOString().split("T")[0];
    this.create();
    /*if(this.data.username) {
      if(this.data.email) {
        if(this.data.password) {
          if(this.data.password_repeat) {
            if(this.data.password == this.data.password_repeat) {
              this.btnDisabled = true;
              this.create();
            } else {
              this.notificationService.alertToast('Las contraseñas deben ser iguales.')
            }
          } else {
            this.notificationService.alertToast('Confirmar contraseña es requerido.')
          }
        } else {
          this.notificationService.alertToast('La contraseña es requerida.')
        }
      } else {
        this.notificationService.alertToast('El correo electrónico es requerido.')
      }
    } else {
      this.notificationService.alertToast('El nombre de usuario es requerido.')
    }*/
  }

  public create() {
    this.usuarioService.create(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Registro', 'Tu usuario se ha registrado exitosamente.');
      this.closeModal();
    }, (err) => {
      console.error(err);
      this.btnDisabled = false;
      if(err.status == '400') {
        this.notificationService.alertToast('El nombre de usuario o correo electrónico ya existe.');
      }
    });
  }
}
