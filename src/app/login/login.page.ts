import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalLoginComponent } from './modal-login/modal-login.component';
import { UsuarioService } from '../_service/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private disabledBtn:boolean = false;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private data = {
    usu_email: '',
    usu_pass: ''
  }

  constructor(private router: Router,
    private location: Location,
    private mainService: UsuarioService,
    private notificacionService: NotificacionService,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  authentication() {
    if(this.data.usu_email) {
      if(this.data.usu_pass) {
        this.auth();
      } else {
        this.notificacionService.alertToast("La contraseña es requerida.");
      }
    } else {
      this.notificacionService.alertToast("El usuario es requerido.");
    }
  }

  auth() {
    //let events = this.events;
    this.disabledBtn = true;
    //this.goToRoute('tutor');
    this.mainService.authentication(this.data)
    .subscribe((res) => {
      console.log(res)
      if(res.usu_tipo == '1') {
        this.goToRoute('admin');
      } else if(res.usu_tipo == '2') {
        this.goToRoute('tutor');
      } else if(res.usu_tipo == '3') {
        this.goToRoute('student');
      }
      localStorage.setItem('currentId', res.usu_id);  
      localStorage.setItem('currentEmail', res.usu_email);
      localStorage.setItem('currentFirstName', res.usu_nombre);
      localStorage.setItem('currentLastName', res.usu_apellido);
      localStorage.setItem('currentPicture', 'https://static.vecteezy.com/system/resources/previews/000/425/957/non_2x/vector-users-icon.jpg');

      /*localStorage.setItem('cart', JSON.stringify([]));
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentId', res.id);             
      localStorage.setItem('currentPicture', res.picture);
      localStorage.setItem('currentFirstName', res.firstname);
      localStorage.setItem('currentLastName', res.lastname);
      localStorage.setItem('currentState', res.state);
      this.goToRoute('cliente/cliente/categoria');*/
      this.disabledBtn = false;
    }, (error) => {
      console.error(error);
      this.disabledBtn = false;
      if(error.status == 401){
        this.notificacionService.alertToast('Usuario o contraseña incorrectos.');
      }else
      if(error.status == 400){
        this.notificacionService.alertToast('Su usuario no esta registrado en esta aplicacion.');
      }
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalLoginComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

}
