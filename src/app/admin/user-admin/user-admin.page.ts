import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalUserComponent } from '../estudiante-admin/modal-user/modal-user.component';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.page.html',
  styleUrls: ['./user-admin.page.scss'],
})
export class UserAdminPage implements OnInit {
  parameter:any;
  search:string;
  table:any[];

  constructor(
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private mainService: UsuarioService,
    private notificationService: NotificacionService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  public getAll(type:any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      res.forEach(element => {
        if(element.usu_tipo == type) {
          this.table.push(element);
        }
      });
      console.log(res)
      this.notificationService.dismiss();
    }, (err) => {
      this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Usuario Eliminado', 'El usuario fue eliminado exitosamente.');
      this.getAll(this.parameter);
    },(error) => {
      console.error(error)
      this.notificationService.alertMessage('Error D:', error.error);
    });
  }

  //DELETE
  async confirmation(data:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar Usuario',
      message: '¿Desea eliminar el usuario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalUserComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getAll(this.parameter);
      }
    });
    return await modal.present();
  }
}
