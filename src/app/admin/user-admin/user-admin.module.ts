import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserAdminPageRoutingModule } from './user-admin-routing.module';
import { UserAdminPage } from './user-admin.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalUserComponent } from '../estudiante-admin/modal-user/modal-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    UserAdminPageRoutingModule
  ],
  declarations: [
    UserAdminPage,
    ModalUserComponent
  ], entryComponents: [
    ModalUserComponent
  ]
})
export class UserAdminPageModule {}
