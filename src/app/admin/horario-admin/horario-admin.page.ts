import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { HorarioService } from 'src/app/_service/horario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalHorarioComponent } from './modal-horario/modal-horario.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-horario-admin',
  templateUrl: './horario-admin.page.html',
  styleUrls: ['./horario-admin.page.scss'],
})
export class HorarioAdminPage implements OnInit {
  table:any[];

  constructor(
    private modalController: ModalController,
    private mainService: HorarioService,
    private notificationService: NotificacionService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  public getAll() {
    //this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      this.table = res;
      console.log(res)
      //this.notificationService.dismiss();
    }, (err) => {
      //this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalHorarioComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  
  async presentActionSheet(id:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [{
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.confirmation(id);
        }
      }, {
        text: 'Editar',
        icon: 'create',
        handler: () => {
          console.log('Share clicked');
          this.presentModal(id);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Curso Eliminado', 'El curso fue eliminado exitosamente.');
      this.getAll();
    },(error) => {
      console.error(error)
      this.notificationService.alertMessage('Error D:', error.error);
    });
  }

  //DELETE
  async confirmation(data:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar Curso',
      message: '¿Desea eliminar el curso?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

}
