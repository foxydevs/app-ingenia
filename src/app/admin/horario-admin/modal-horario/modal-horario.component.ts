import { Component, OnInit } from '@angular/core';
import { HorarioService } from 'src/app/_service/horario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-horario',
  templateUrl: './modal-horario.component.html',
  styleUrls: ['./modal-horario.component.scss'],
})
export class ModalHorarioComponent implements OnInit {
  data = {
    horario:'',
    id: 0
  }
  btnDisabled:boolean;
  contador:number = 1;
  parameter:any;
  constructor(
    private mainService: HorarioService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    //console.log(this.navParams.get('value'))
    this.parameter = this.navParams.get('value');
    if(this.parameter) {
      this.getSingle(+this.navParams.get('value'));
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  guardar() {
    this.create(this.data);
  }

  saveChanges() {
    if(this.data.horario) {
        this.btnDisabled = true;
        if(this.parameter) {
          this.update();
        } else {
          this.create(this.data);
        }
    } else {
      this.notificationService.alertToast('El horario es requerido.');
    }
  }

  public create(data:any) {
    
    console.log('Entro veces', this.contador)
    this.contador++;
    this.mainService.create(data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Horario Agregado', 'El horario ha sido agregado exitosamente.');
      this.modalController.dismiss(res);
      this.btnDisabled = false;
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public update() {
    this.mainService.update(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Horario Actualizado', 'El horario ha sido actualizado exitosamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);      
    });
  }

  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      this.data = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

}
