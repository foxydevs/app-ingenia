import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HorarioAdminPageRoutingModule } from './horario-admin-routing.module';

import { HorarioAdminPage } from './horario-admin.page';
import { ModalHorarioComponent } from './modal-horario/modal-horario.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HorarioAdminPageRoutingModule
  ],
  declarations: [
    HorarioAdminPage,
    ModalHorarioComponent
  ], entryComponents: [
    ModalHorarioComponent
  ]
})
export class HorarioAdminPageModule {}
