import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HorarioAdminPage } from './horario-admin.page';

const routes: Routes = [
  {
    path: '',
    component: HorarioAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HorarioAdminPageRoutingModule {}
