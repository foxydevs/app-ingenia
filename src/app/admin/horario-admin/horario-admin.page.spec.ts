import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HorarioAdminPage } from './horario-admin.page';

describe('HorarioAdminPage', () => {
  let component: HorarioAdminPage;
  let fixture: ComponentFixture<HorarioAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorarioAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HorarioAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
