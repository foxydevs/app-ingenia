import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SucursalAdminPage } from './sucursal-admin.page';

describe('SucursalAdminPage', () => {
  let component: SucursalAdminPage;
  let fixture: ComponentFixture<SucursalAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SucursalAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SucursalAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
