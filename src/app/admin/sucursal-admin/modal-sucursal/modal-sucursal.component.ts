import { Component, OnInit } from '@angular/core';
import { SucursalService } from 'src/app/_service/sucursal.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-sucursal',
  templateUrl: './modal-sucursal.component.html',
  styleUrls: ['./modal-sucursal.component.scss'],
})
export class ModalSucursalComponent implements OnInit {
  data = {
  	sucursales_titulo: '',
  	sucursales_descripcion: '',
  	sucursales_direccion: '',
  	sucursales_telefono: '',
  	sucursales_whatsapp: ''
  }
  btnDisabled:boolean;

  constructor(
    private mainService: SucursalService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    if(this.navParams.get('value')) {
      this.getSingle(+this.navParams.get('value'));
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  saveChanges() {
    //this.data.cursos_fecha = new Date().toISOString().split("T")[0];
    if(this.data.sucursales_titulo) {
      if(this.data.sucursales_descripcion) {
        if(this.navParams.get('value')) {
          this.update();
        } else {
          this.create();
        }
      } else {
        this.notificationService.alertToast('La descripcion es requerida.');
      }
    } else {
      this.notificationService.alertToast('El titulo es requerida.');
    }
    
    console.log(this.data)
  }

  public create() {
    this.mainService.create(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Agregado', 'El curso ha sido agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public update() {
    this.mainService.update(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Actualizado', 'El curso ha sido actualizado exitosamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);      
    });
  }

  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      this.data = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

}
