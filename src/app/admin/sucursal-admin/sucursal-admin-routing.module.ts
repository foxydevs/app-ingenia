import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SucursalAdminPage } from './sucursal-admin.page';

const routes: Routes = [
  {
    path: '',
    component: SucursalAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SucursalAdminPageRoutingModule {}
