import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SucursalAdminPageRoutingModule } from './sucursal-admin-routing.module';
import { SucursalAdminPage } from './sucursal-admin.page';
import { ModalSucursalComponent } from './modal-sucursal/modal-sucursal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SucursalAdminPageRoutingModule
  ],
  declarations: [
    SucursalAdminPage,
    ModalSucursalComponent
  ], entryComponents: [
    ModalSucursalComponent
  ]
})
export class SucursalAdminPageModule {}
