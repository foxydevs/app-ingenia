import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { SucursalService } from 'src/app/_service/sucursal.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalSucursalComponent } from './modal-sucursal/modal-sucursal.component';

@Component({
  selector: 'app-sucursal-admin',
  templateUrl: './sucursal-admin.page.html',
  styleUrls: ['./sucursal-admin.page.scss'],
})
export class SucursalAdminPage implements OnInit {
  table:any[];

  constructor(
    private modalController: ModalController,
    private mainService: SucursalService,
    private notificationService: NotificacionService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }

  public getAll() {
    //this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      this.table = res;
      console.log(res)
      //this.notificationService.dismiss();
    }, (err) => {
      //this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalSucursalComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  
  async presentActionSheet(id:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [{
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.confirmation(id);
        }
      }, {
        text: 'Editar',
        icon: 'create',
        handler: () => {
          console.log('Share clicked');
          this.presentModal(id);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Sucursal Eliminado', 'El sucursal fue eliminado exitosamente.');
      this.getAll();
    },(error) => {
      console.error(error)
      this.notificationService.alertMessage('Error D:', error.error);
    });
  }

  //DELETE
  async confirmation(data:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar Sucursal',
      message: '¿Desea eliminar el sucursal?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }
}
