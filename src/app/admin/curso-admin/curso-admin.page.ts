import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { ModalCursoComponent } from './modal-curso/modal-curso.component';
import { CursoService } from 'src/app/_service/curso.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-curso-admin',
  templateUrl: './curso-admin.page.html',
  styleUrls: ['./curso-admin.page.scss'],
})
export class CursoAdminPage implements OnInit {
  table:any[];

  constructor(
    private modalController: ModalController,
    private mainService: CursoService,
    private notificationService: NotificacionService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }

  public getAll() {
    //this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      this.table = res;
      console.log(res)
      //this.notificationService.dismiss();
    }, (err) => {
      //this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalCursoComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  
  async presentActionSheet(id:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [{
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.confirmation(id);
        }
      }, {
        text: 'Editar',
        icon: 'create',
        handler: () => {
          console.log('Share clicked');
          this.presentModal(id);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Curso Eliminado', 'El curso fue eliminado exitosamente.');
      this.getAll();
    },(error) => {
      console.error(error)
      this.notificationService.alertMessage('Error D:', error.error);
    });
  }

  //DELETE
  async confirmation(data:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar Curso',
      message: '¿Desea eliminar el curso?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

}
