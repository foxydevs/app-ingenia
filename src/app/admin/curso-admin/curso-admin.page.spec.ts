import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CursoAdminPage } from './curso-admin.page';

describe('CursoAdminPage', () => {
  let component: CursoAdminPage;
  let fixture: ComponentFixture<CursoAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CursoAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CursoAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
