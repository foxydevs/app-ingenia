import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursoAdminPage } from './curso-admin.page';

const routes: Routes = [
  {
    path: '',
    component: CursoAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CursoAdminPageRoutingModule {}
