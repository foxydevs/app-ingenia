import { Component, OnInit } from '@angular/core';
import { CursoService } from 'src/app/_service/curso.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-curso',
  templateUrl: './modal-curso.component.html',
  styleUrls: ['./modal-curso.component.scss'],
})
export class ModalCursoComponent implements OnInit {
  data = {
    cursos_titulo:'',
    cursos_descripcion:'',
    cursos_usu_id: +localStorage.getItem('currentId'),
    cursos_fecha:'',
    cursos_estado:1
  }
  btnDisabled:boolean;
  contador:number = 1;
  parameter:any;
  constructor(
    private mainService: CursoService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    //console.log(this.navParams.get('value'))
    this.parameter = this.navParams.get('value');
    if(this.parameter) {
      this.getSingle(+this.navParams.get('value'));
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  guardar() {
    this.create(this.data);
  }

  saveChanges() {
    this.data.cursos_fecha = new Date().toISOString().split("T")[0];
    console.log(this.data)

    if(this.data.cursos_titulo) {
      if(this.data.cursos_descripcion) {
        this.btnDisabled = true;
        if(this.parameter) {
          this.update();
        } else {
          this.create(this.data);
        }
      } else {
        this.notificationService.alertToast('La descripcion es requerida.');
      }
    } else {
      this.notificationService.alertToast('El titulo es requerida.');
    }
  }

  public create(data:any) {
    
    console.log('Entro veces', this.contador)
    this.contador++;
    this.mainService.create(data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Agregado', 'El curso ha sido agregado exitosamente.');
      this.modalController.dismiss(res);
      this.btnDisabled = false;
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public update() {
    this.mainService.update(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Actualizado', 'El curso ha sido actualizado exitosamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);      
    });
  }

  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      this.data = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

}
