import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CursoAdminPageRoutingModule } from './curso-admin-routing.module';

import { CursoAdminPage } from './curso-admin.page';
import { ModalCursoComponent } from './modal-curso/modal-curso.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CursoAdminPageRoutingModule
  ],
  declarations: [
    CursoAdminPage,
    ModalCursoComponent
  ], entryComponents:[
    ModalCursoComponent
  ]
})
export class CursoAdminPageModule {}
