import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstudianteAdminPage } from './estudiante-admin.page';

const routes: Routes = [
  {
    path: '',
    component: EstudianteAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstudianteAdminPageRoutingModule {}
