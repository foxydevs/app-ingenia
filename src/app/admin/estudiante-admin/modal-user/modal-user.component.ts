import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-user',
  templateUrl: './modal-user.component.html',
  styleUrls: ['./modal-user.component.scss'],
})
export class ModalUserComponent implements OnInit {
  data:any = {
    usu_id: 0,
    usu_nombre: '',
    usu_apellido: '',
    usu_email: '',
    usu_matricula:'',
    usu_ocupacion_id: 0,
    usu_empresa:'',//
    usu_cargo:'',//
    usu_antiguedad:'',//
    usu_lugar_estudios:'',//
    usu_carrera:'',//
    usu_nivel_ano:'',//
    usu_direccion_casa: '',//
    usu_profesion: '',//
    usu_tel: '',//
    usu_cel: '',//
    usu_fech_nacimiento: '',//
    usu_inviertes_letras: '',//
    usu_dificultad_lectura: '',//
    usu_dificultad_atencion:'',//
    usu_problema_visual:'',//
    usu_problema_significado_oracion:'',//
    usu_usu_lees_por_que_id: 0,//
    usu_libros_anuales: '',//
    usu_lateralidad: '',//
    usu_capacidad_memoria:'',//
    usu_observaciones: '',
    usu_por_que:'',
    usu_para_que: '',
    usu_para_cuando:'',
    usu_fecha: '',
    usu_estado:1,
    usu_pass: '',
    usu_tipo: 3
  }
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  btnDisabled:boolean;
  contador:number = 1;
  parameter:any;
  constructor(
    private mainService: UsuarioService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    //console.log(this.navParams.get('value'))
    this.parameter = this.navParams.get('value');
    if(this.parameter) {
      this.getSingle(+this.navParams.get('value'));
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  guardar() {
    this.create(this.data);
  }

  saveChanges() {
    this.data.usu_fecha = new Date().toISOString().split("T")[0];
    console.log(this.data)
    if(this.data.usu_nombre) {
      if(this.data.usu_apellido) {
        if(this.data.usu_email) {
          if(this.parameter) {
            this.update();
          } else {
            this.create(this.data);
          }
        } else {
          this.notificationService.alertToast('El correo es requerido.')
        }
      } else {
        this.notificationService.alertToast('El apellido es requerido.')
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.')
    }    
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  public create(data:any) {
    console.log('Entro veces', this.contador)
    this.contador++;
    this.mainService.create(data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Agregado', 'El curso ha sido agregado exitosamente.');
      this.modalController.dismiss(res);
      this.btnDisabled = false;
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);
    });
  }

  public update() {
    this.mainService.update(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Curso Actualizado', 'El curso ha sido actualizado exitosamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      this.btnDisabled = false;
      this.notificationService.alertMessage('Error D:', err);      
    });
  }

  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res)=> {
      this.data = res;
    }, (err) => {
      this.notificationService.alertMessage('Error D:', err);
    });
  }

}
