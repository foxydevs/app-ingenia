import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EstudianteAdminPage } from './estudiante-admin.page';

describe('EstudianteAdminPage', () => {
  let component: EstudianteAdminPage;
  let fixture: ComponentFixture<EstudianteAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstudianteAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EstudianteAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
