import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-estudiante-admin',
  templateUrl: './estudiante-admin.page.html',
  styleUrls: ['./estudiante-admin.page.scss'],
})
export class EstudianteAdminPage implements OnInit {

  constructor(
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }


}
