import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstudianteAdminPageRoutingModule } from './estudiante-admin-routing.module';

import { EstudianteAdminPage } from './estudiante-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstudianteAdminPageRoutingModule
  ],
  declarations: [EstudianteAdminPage]
})
export class EstudianteAdminPageModule {}
