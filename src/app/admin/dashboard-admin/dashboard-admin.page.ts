import { Component, OnInit } from '@angular/core';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { CobroService } from 'src/app/_service/cobro.service';
import { ModalController } from '@ionic/angular';
import { ModalCobroComponent } from 'src/app/student/cobros-student/modal-cobro/modal-cobro.component';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.page.html',
  styleUrls: ['./dashboard-admin.page.scss'],
})
export class DashboardAdminPage implements OnInit {
  private table:any[];
  
  constructor(
    private notificationService: NotificacionService,
    private mainService: CobroService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getAll();
  }

  public getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe((res)=> {
      this.table = [];
      this.table = res;
      console.log(res)
      this.notificationService.dismiss();
    }, (err) => {
      this.notificationService.dismiss();
      this.notificationService.alertMessage('Error D:', err.error);
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalCobroComponent,
      componentProps: {
        value: id,
        admin: 'Admin'
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getAll();
      }
    });
    return await modal.present();
  }
}
