import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPage } from './admin.page';
const routes: Routes = [
  {
    path: '',
    component: AdminPage,
    children: [
      {
        path: 'dashboard-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./dashboard-admin/dashboard-admin.module').then( m => m.DashboardAdminPageModule)
          }
        ]
      },
      {
        path: 'curso-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./curso-admin/curso-admin.module').then( m => m.CursoAdminPageModule)
          }
        ]
      },
      {
        path: 'sucursal-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./sucursal-admin/sucursal-admin.module').then( m => m.SucursalAdminPageModule)
          }
        ]
      },
      {
        path: 'perfil-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('./perfil-admin/perfil-admin.module').then( m => m.PerfilAdminPageModule)
          }
        ]
      },
      {
        path: 'estudiante-admin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./estudiante-admin/estudiante-admin.module').then( m => m.EstudianteAdminPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/admin/dashboard-admin',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/admin/dashboard-admin',
    pathMatch: 'full'
  },
  {
    path: 'user-admin/:id',
    loadChildren: () => import('./user-admin/user-admin.module').then( m => m.UserAdminPageModule)
  },
  {
    path: 'horario-admin',
    loadChildren: () => import('./horario-admin/horario-admin.module').then( m => m.HorarioAdminPageModule)
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule {}
